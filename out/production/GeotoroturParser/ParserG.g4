parser grammar ParserG;

options {
  tokenVocab = Scanner;
}

program : consulta EOF                                                                                                  #programAST;
consulta : LLAVEIZQUIERDA cuerpoconsulta LLAVEDERECHA                                                                   #consultaAST;

cuerpoconsulta : atributos
                 COMA capas
                 COMA filtro
                 COMA ordenamientos
                 COMA agrupamientos
                 COMA motor                                                                                             #cuerpoconsultaAST;

atributos : ATRIBUTOS DOSPUNTOS listaatributos                                                                          #atributosCCAST;//cuerpoconsulta

listaatributos : CORCHETEIZQUIERDO atributo (COMA atributo)*  CORCHETEDERECHO                                           #listaatributosACCAST; //atributos cuerpoconsulta

atributo : LLAVEIZQUIERDA ATRIBUTO DOSPUNTOS opcionesatributo
            COMA RENOMBRE DOSPUNTOS TEXTO LLAVEDERECHA                                                                  #atributoLAACCAST;//listaatributos atributos cuerpoconsulta

opcionesatributo: TEXTO                                                                                                 #textoALAACCAST
                | NUMERO                                                                                                #numeroALAACCAST
                | atributocapa                                                                                          #atributocapaALAACCAST
                | consulta                                                                                              #consultaALAACCAST
                | cuerpoexpresion                                                                                       #cuerpoexpresionALAACCAST
                | expresionlogica                                                                                       #expresionlogicaALAACCAST;//atributo listaatributos atributos cuerpoconsulta

atributocapa : LLAVEIZQUIERDA CAPA DOSPUNTOS TEXTO COMA ATRIBUTO DOSPUNTOS TEXTO LLAVEDERECHA                           #atributocapaOAALAACCAST;//opcionesatributo atributo listaatributos atributos cuerpoconsulta

expresion : LLAVEIZQUIERDA cuerpoexpresion LLAVEDERECHA                                                                 #expresionAST;

cuerpoexpresion : OP DOSPUNTOS TEXTO COMA OPERANDOS DOSPUNTOS listaatributos                                            #cuerpoexpresionEAST;//expresion

expresionlogica : (NOMBRECOLUMNA DOSPUNTOS TEXTO)                                                                       #nombrecolumnaELAST//expresion logica
                | expresion                                                                                             #expresionELAST
                | expresionlogicaoplog                                                                                  #expresionlogicaoplogELAST;

expresionlogicaoplog : LLAVEIZQUIERDA OPLOG DOSPUNTOS TEXTO COMA OPERANDOS DOSPUNTOS listaatributos LLAVEDERECHA        #expresionlogicaoplogELOLELAST;//expresion logica op log expresion logica

capas : CAPAS DOSPUNTOS opcionescapa                                                                                    #capasAST;

opcionescapa : TEXTO                                                                                                    #capavaciaCAST //capa
            | (CORCHETEIZQUIERDO repeticioncapa CORCHETEDERECHO)                                                        #repeticioncapaCAST;

repeticioncapa : capa (COMA capa)*                                                                                      #repeticioncapaRCCAST; //repeticioncapa capa

capa : LLAVEIZQUIERDA CAPA DOSPUNTOS opcioncapa
       COMA RENOMBRE DOSPUNTOS TEXTO LLAVEDERECHA                                                                       #capaRCRCCAST;//repeticioncapa repeticioncapa capa

opcioncapa : TEXTO                                                                                                      #textoOCCRCRCCAST//opcioncapa capa repeticioncapa repeticioncapa capa
            | consulta                                                                                                  #consultaOCCRCRCCAST;

filtro : FILTRO DOSPUNTOS opcionfiltro                                                                                  #filtroAST;

opcionfiltro : TEXTO                                                                                                    #textoOFFAST
            | expresionlogica                                                                                           #expresionlogicaOFFAST;//opcionfiltro filtro

opcionesordenamientosagrupamientos: TEXTO                                                                               #textoOOAAST
                                    | atributos                                                                         #atributosOOAAST;

ordenamientos : ORDENAMIENTOS DOSPUNTOS opcionesordenamientosagrupamientos                                              #ordenamientosAST;

agrupamientos : AGRUPAMIENTOS DOSPUNTOS opcionesordenamientosagrupamientos                                              #agrupamientosAST;

motor : MOTOR DOSPUNTOS opcionesmotor                                                                                   #motorAST;

opcionesmotor : MONGO                                                                                                   #mongoOMAST//opcionesmotor motor
                | POSTGRES                                                                                              #postgresOMAST
                | TEXTO                                                                                                 #textoOMAST;



