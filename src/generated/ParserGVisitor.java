// Generated from /home/fabiancastrob/Repos/GeotoroturParser/src/ParserG.g4 by ANTLR 4.7
package generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ParserG}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ParserGVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code programAST}
	 * labeled alternative in {@link ParserG#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgramAST(ParserG.ProgramASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code consultaAST}
	 * labeled alternative in {@link ParserG#consulta}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConsultaAST(ParserG.ConsultaASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cuerpoconsultaAST}
	 * labeled alternative in {@link ParserG#cuerpoconsulta}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCuerpoconsultaAST(ParserG.CuerpoconsultaASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atributosCCAST}
	 * labeled alternative in {@link ParserG#atributos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtributosCCAST(ParserG.AtributosCCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listaatributosACCAST}
	 * labeled alternative in {@link ParserG#listaatributos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListaatributosACCAST(ParserG.ListaatributosACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atributoLAACCAST}
	 * labeled alternative in {@link ParserG#atributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtributoLAACCAST(ParserG.AtributoLAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code textoALAACCAST}
	 * labeled alternative in {@link ParserG#opcionesatributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTextoALAACCAST(ParserG.TextoALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numeroALAACCAST}
	 * labeled alternative in {@link ParserG#opcionesatributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeroALAACCAST(ParserG.NumeroALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atributocapaALAACCAST}
	 * labeled alternative in {@link ParserG#opcionesatributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtributocapaALAACCAST(ParserG.AtributocapaALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code consultaALAACCAST}
	 * labeled alternative in {@link ParserG#opcionesatributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConsultaALAACCAST(ParserG.ConsultaALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cuerpoexpresionALAACCAST}
	 * labeled alternative in {@link ParserG#opcionesatributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCuerpoexpresionALAACCAST(ParserG.CuerpoexpresionALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionlogicaALAACCAST}
	 * labeled alternative in {@link ParserG#opcionesatributo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionlogicaALAACCAST(ParserG.ExpresionlogicaALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atributocapaOAALAACCAST}
	 * labeled alternative in {@link ParserG#atributocapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtributocapaOAALAACCAST(ParserG.AtributocapaOAALAACCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionAST}
	 * labeled alternative in {@link ParserG#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionAST(ParserG.ExpresionASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cuerpoexpresionEAST}
	 * labeled alternative in {@link ParserG#cuerpoexpresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCuerpoexpresionEAST(ParserG.CuerpoexpresionEASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nombrecolumnaELAST}
	 * labeled alternative in {@link ParserG#expresionlogica}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNombrecolumnaELAST(ParserG.NombrecolumnaELASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionELAST}
	 * labeled alternative in {@link ParserG#expresionlogica}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionELAST(ParserG.ExpresionELASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionlogicaoplogELAST}
	 * labeled alternative in {@link ParserG#expresionlogica}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionlogicaoplogELAST(ParserG.ExpresionlogicaoplogELASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionlogicaoplogELOLELAST}
	 * labeled alternative in {@link ParserG#expresionlogicaoplog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionlogicaoplogELOLELAST(ParserG.ExpresionlogicaoplogELOLELASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code capasAST}
	 * labeled alternative in {@link ParserG#capas}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCapasAST(ParserG.CapasASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code capavaciaCAST}
	 * labeled alternative in {@link ParserG#opcionescapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCapavaciaCAST(ParserG.CapavaciaCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repeticioncapaCAST}
	 * labeled alternative in {@link ParserG#opcionescapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeticioncapaCAST(ParserG.RepeticioncapaCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repeticioncapaRCCAST}
	 * labeled alternative in {@link ParserG#repeticioncapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepeticioncapaRCCAST(ParserG.RepeticioncapaRCCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code capaRCRCCAST}
	 * labeled alternative in {@link ParserG#capa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCapaRCRCCAST(ParserG.CapaRCRCCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code textoOCCRCRCCAST}
	 * labeled alternative in {@link ParserG#opcioncapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTextoOCCRCRCCAST(ParserG.TextoOCCRCRCCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code consultaOCCRCRCCAST}
	 * labeled alternative in {@link ParserG#opcioncapa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConsultaOCCRCRCCAST(ParserG.ConsultaOCCRCRCCASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code filtroAST}
	 * labeled alternative in {@link ParserG#filtro}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFiltroAST(ParserG.FiltroASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code textoOFFAST}
	 * labeled alternative in {@link ParserG#opcionfiltro}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTextoOFFAST(ParserG.TextoOFFASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expresionlogicaOFFAST}
	 * labeled alternative in {@link ParserG#opcionfiltro}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionlogicaOFFAST(ParserG.ExpresionlogicaOFFASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code textoOOAAST}
	 * labeled alternative in {@link ParserG#opcionesordenamientosagrupamientos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTextoOOAAST(ParserG.TextoOOAASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code atributosOOAAST}
	 * labeled alternative in {@link ParserG#opcionesordenamientosagrupamientos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtributosOOAAST(ParserG.AtributosOOAASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ordenamientosAST}
	 * labeled alternative in {@link ParserG#ordenamientos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdenamientosAST(ParserG.OrdenamientosASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code agrupamientosAST}
	 * labeled alternative in {@link ParserG#agrupamientos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgrupamientosAST(ParserG.AgrupamientosASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code motorAST}
	 * labeled alternative in {@link ParserG#motor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMotorAST(ParserG.MotorASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mongoOMAST}
	 * labeled alternative in {@link ParserG#opcionesmotor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMongoOMAST(ParserG.MongoOMASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postgresOMAST}
	 * labeled alternative in {@link ParserG#opcionesmotor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostgresOMAST(ParserG.PostgresOMASTContext ctx);
	/**
	 * Visit a parse tree produced by the {@code textoOMAST}
	 * labeled alternative in {@link ParserG#opcionesmotor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTextoOMAST(ParserG.TextoOMASTContext ctx);
}