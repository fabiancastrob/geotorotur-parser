// Generated from /home/fabiancastrob/Repos/GeotoroturParser/src/ParserG.g4 by ANTLR 4.7
package generated;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ParserG extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		DOSPUNTOS=1, LLAVEIZQUIERDA=2, LLAVEDERECHA=3, COMA=4, CORCHETEIZQUIERDO=5, 
		CORCHETEDERECHO=6, BUFFER=7, INTERSECT=8, NEAR=9, DISTANCE=10, DISTINCT=11, 
		WITHIN=12, UPPER=13, CONCATENAR=14, ATRIBUTOS=15, ATRIBUTO=16, CAPAS=17, 
		CAPA=18, FILTRO=19, ORDENAMIENTOS=20, AGRUPAMIENTOS=21, RENOMBRE=22, OP=23, 
		OPERANDOS=24, OPLOG=25, NOMBRECOLUMNA=26, MOTOR=27, MONGO=28, POSTGRES=29, 
		NUMERO=30, TEXTO=31, ESPACIOENBLANCO=32;
	public static final int
		RULE_program = 0, RULE_consulta = 1, RULE_cuerpoconsulta = 2, RULE_atributos = 3, 
		RULE_listaatributos = 4, RULE_atributo = 5, RULE_opcionesatributo = 6, 
		RULE_atributocapa = 7, RULE_expresion = 8, RULE_cuerpoexpresion = 9, RULE_expresionlogica = 10, 
		RULE_expresionlogicaoplog = 11, RULE_capas = 12, RULE_opcionescapa = 13, 
		RULE_repeticioncapa = 14, RULE_capa = 15, RULE_opcioncapa = 16, RULE_filtro = 17, 
		RULE_opcionfiltro = 18, RULE_opcionesordenamientosagrupamientos = 19, 
		RULE_ordenamientos = 20, RULE_agrupamientos = 21, RULE_motor = 22, RULE_opcionesmotor = 23;
	public static final String[] ruleNames = {
		"program", "consulta", "cuerpoconsulta", "atributos", "listaatributos", 
		"atributo", "opcionesatributo", "atributocapa", "expresion", "cuerpoexpresion", 
		"expresionlogica", "expresionlogicaoplog", "capas", "opcionescapa", "repeticioncapa", 
		"capa", "opcioncapa", "filtro", "opcionfiltro", "opcionesordenamientosagrupamientos", 
		"ordenamientos", "agrupamientos", "motor", "opcionesmotor"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "':'", "'{'", "'}'", "','", "'['", "']'", "'buffer'", "'interserct'", 
		"'near'", "'distance'", "'distinct'", "'within'", "'upper'", "'concatenar'", 
		"'atributos'", "'atributo'", "'capas'", "'capa'", "'filtro'", "'ordenamientos'", 
		"'agrupamientos'", "'renombre'", "'op'", "'operandos'", "'opLog'", "'nombreColumna'", 
		"'motor'", "'Mongo'", "'PostgreSQL'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "DOSPUNTOS", "LLAVEIZQUIERDA", "LLAVEDERECHA", "COMA", "CORCHETEIZQUIERDO", 
		"CORCHETEDERECHO", "BUFFER", "INTERSECT", "NEAR", "DISTANCE", "DISTINCT", 
		"WITHIN", "UPPER", "CONCATENAR", "ATRIBUTOS", "ATRIBUTO", "CAPAS", "CAPA", 
		"FILTRO", "ORDENAMIENTOS", "AGRUPAMIENTOS", "RENOMBRE", "OP", "OPERANDOS", 
		"OPLOG", "NOMBRECOLUMNA", "MOTOR", "MONGO", "POSTGRES", "NUMERO", "TEXTO", 
		"ESPACIOENBLANCO"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ParserG.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ParserG(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	 
		public ProgramContext() { }
		public void copyFrom(ProgramContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ProgramASTContext extends ProgramContext {
		public ConsultaContext consulta() {
			return getRuleContext(ConsultaContext.class,0);
		}
		public TerminalNode EOF() { return getToken(ParserG.EOF, 0); }
		public ProgramASTContext(ProgramContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitProgramAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			_localctx = new ProgramASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			consulta();
			setState(49);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConsultaContext extends ParserRuleContext {
		public ConsultaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_consulta; }
	 
		public ConsultaContext() { }
		public void copyFrom(ConsultaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ConsultaASTContext extends ConsultaContext {
		public TerminalNode LLAVEIZQUIERDA() { return getToken(ParserG.LLAVEIZQUIERDA, 0); }
		public CuerpoconsultaContext cuerpoconsulta() {
			return getRuleContext(CuerpoconsultaContext.class,0);
		}
		public TerminalNode LLAVEDERECHA() { return getToken(ParserG.LLAVEDERECHA, 0); }
		public ConsultaASTContext(ConsultaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitConsultaAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConsultaContext consulta() throws RecognitionException {
		ConsultaContext _localctx = new ConsultaContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_consulta);
		try {
			_localctx = new ConsultaASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			match(LLAVEIZQUIERDA);
			setState(52);
			cuerpoconsulta();
			setState(53);
			match(LLAVEDERECHA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CuerpoconsultaContext extends ParserRuleContext {
		public CuerpoconsultaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cuerpoconsulta; }
	 
		public CuerpoconsultaContext() { }
		public void copyFrom(CuerpoconsultaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CuerpoconsultaASTContext extends CuerpoconsultaContext {
		public AtributosContext atributos() {
			return getRuleContext(AtributosContext.class,0);
		}
		public List<TerminalNode> COMA() { return getTokens(ParserG.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(ParserG.COMA, i);
		}
		public CapasContext capas() {
			return getRuleContext(CapasContext.class,0);
		}
		public FiltroContext filtro() {
			return getRuleContext(FiltroContext.class,0);
		}
		public OrdenamientosContext ordenamientos() {
			return getRuleContext(OrdenamientosContext.class,0);
		}
		public AgrupamientosContext agrupamientos() {
			return getRuleContext(AgrupamientosContext.class,0);
		}
		public MotorContext motor() {
			return getRuleContext(MotorContext.class,0);
		}
		public CuerpoconsultaASTContext(CuerpoconsultaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitCuerpoconsultaAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CuerpoconsultaContext cuerpoconsulta() throws RecognitionException {
		CuerpoconsultaContext _localctx = new CuerpoconsultaContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_cuerpoconsulta);
		try {
			_localctx = new CuerpoconsultaASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			atributos();
			setState(56);
			match(COMA);
			setState(57);
			capas();
			setState(58);
			match(COMA);
			setState(59);
			filtro();
			setState(60);
			match(COMA);
			setState(61);
			ordenamientos();
			setState(62);
			match(COMA);
			setState(63);
			agrupamientos();
			setState(64);
			match(COMA);
			setState(65);
			motor();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtributosContext extends ParserRuleContext {
		public AtributosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atributos; }
	 
		public AtributosContext() { }
		public void copyFrom(AtributosContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtributosCCASTContext extends AtributosContext {
		public TerminalNode ATRIBUTOS() { return getToken(ParserG.ATRIBUTOS, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public ListaatributosContext listaatributos() {
			return getRuleContext(ListaatributosContext.class,0);
		}
		public AtributosCCASTContext(AtributosContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitAtributosCCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtributosContext atributos() throws RecognitionException {
		AtributosContext _localctx = new AtributosContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_atributos);
		try {
			_localctx = new AtributosCCASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			match(ATRIBUTOS);
			setState(68);
			match(DOSPUNTOS);
			setState(69);
			listaatributos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListaatributosContext extends ParserRuleContext {
		public ListaatributosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listaatributos; }
	 
		public ListaatributosContext() { }
		public void copyFrom(ListaatributosContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ListaatributosACCASTContext extends ListaatributosContext {
		public TerminalNode CORCHETEIZQUIERDO() { return getToken(ParserG.CORCHETEIZQUIERDO, 0); }
		public List<AtributoContext> atributo() {
			return getRuleContexts(AtributoContext.class);
		}
		public AtributoContext atributo(int i) {
			return getRuleContext(AtributoContext.class,i);
		}
		public TerminalNode CORCHETEDERECHO() { return getToken(ParserG.CORCHETEDERECHO, 0); }
		public List<TerminalNode> COMA() { return getTokens(ParserG.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(ParserG.COMA, i);
		}
		public ListaatributosACCASTContext(ListaatributosContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitListaatributosACCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListaatributosContext listaatributos() throws RecognitionException {
		ListaatributosContext _localctx = new ListaatributosContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_listaatributos);
		int _la;
		try {
			_localctx = new ListaatributosACCASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			match(CORCHETEIZQUIERDO);
			setState(72);
			atributo();
			setState(77);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(73);
				match(COMA);
				setState(74);
				atributo();
				}
				}
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(80);
			match(CORCHETEDERECHO);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtributoContext extends ParserRuleContext {
		public AtributoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atributo; }
	 
		public AtributoContext() { }
		public void copyFrom(AtributoContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtributoLAACCASTContext extends AtributoContext {
		public TerminalNode LLAVEIZQUIERDA() { return getToken(ParserG.LLAVEIZQUIERDA, 0); }
		public TerminalNode ATRIBUTO() { return getToken(ParserG.ATRIBUTO, 0); }
		public List<TerminalNode> DOSPUNTOS() { return getTokens(ParserG.DOSPUNTOS); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(ParserG.DOSPUNTOS, i);
		}
		public OpcionesatributoContext opcionesatributo() {
			return getRuleContext(OpcionesatributoContext.class,0);
		}
		public TerminalNode COMA() { return getToken(ParserG.COMA, 0); }
		public TerminalNode RENOMBRE() { return getToken(ParserG.RENOMBRE, 0); }
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TerminalNode LLAVEDERECHA() { return getToken(ParserG.LLAVEDERECHA, 0); }
		public AtributoLAACCASTContext(AtributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitAtributoLAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtributoContext atributo() throws RecognitionException {
		AtributoContext _localctx = new AtributoContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_atributo);
		try {
			_localctx = new AtributoLAACCASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(82);
			match(LLAVEIZQUIERDA);
			setState(83);
			match(ATRIBUTO);
			setState(84);
			match(DOSPUNTOS);
			setState(85);
			opcionesatributo();
			setState(86);
			match(COMA);
			setState(87);
			match(RENOMBRE);
			setState(88);
			match(DOSPUNTOS);
			setState(89);
			match(TEXTO);
			setState(90);
			match(LLAVEDERECHA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpcionesatributoContext extends ParserRuleContext {
		public OpcionesatributoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opcionesatributo; }
	 
		public OpcionesatributoContext() { }
		public void copyFrom(OpcionesatributoContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CuerpoexpresionALAACCASTContext extends OpcionesatributoContext {
		public CuerpoexpresionContext cuerpoexpresion() {
			return getRuleContext(CuerpoexpresionContext.class,0);
		}
		public CuerpoexpresionALAACCASTContext(OpcionesatributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitCuerpoexpresionALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumeroALAACCASTContext extends OpcionesatributoContext {
		public TerminalNode NUMERO() { return getToken(ParserG.NUMERO, 0); }
		public NumeroALAACCASTContext(OpcionesatributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitNumeroALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpresionlogicaALAACCASTContext extends OpcionesatributoContext {
		public ExpresionlogicaContext expresionlogica() {
			return getRuleContext(ExpresionlogicaContext.class,0);
		}
		public ExpresionlogicaALAACCASTContext(OpcionesatributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitExpresionlogicaALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConsultaALAACCASTContext extends OpcionesatributoContext {
		public ConsultaContext consulta() {
			return getRuleContext(ConsultaContext.class,0);
		}
		public ConsultaALAACCASTContext(OpcionesatributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitConsultaALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TextoALAACCASTContext extends OpcionesatributoContext {
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TextoALAACCASTContext(OpcionesatributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitTextoALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AtributocapaALAACCASTContext extends OpcionesatributoContext {
		public AtributocapaContext atributocapa() {
			return getRuleContext(AtributocapaContext.class,0);
		}
		public AtributocapaALAACCASTContext(OpcionesatributoContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitAtributocapaALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpcionesatributoContext opcionesatributo() throws RecognitionException {
		OpcionesatributoContext _localctx = new OpcionesatributoContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_opcionesatributo);
		try {
			setState(98);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new TextoALAACCASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(92);
				match(TEXTO);
				}
				break;
			case 2:
				_localctx = new NumeroALAACCASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(93);
				match(NUMERO);
				}
				break;
			case 3:
				_localctx = new AtributocapaALAACCASTContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(94);
				atributocapa();
				}
				break;
			case 4:
				_localctx = new ConsultaALAACCASTContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(95);
				consulta();
				}
				break;
			case 5:
				_localctx = new CuerpoexpresionALAACCASTContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(96);
				cuerpoexpresion();
				}
				break;
			case 6:
				_localctx = new ExpresionlogicaALAACCASTContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(97);
				expresionlogica();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtributocapaContext extends ParserRuleContext {
		public AtributocapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atributocapa; }
	 
		public AtributocapaContext() { }
		public void copyFrom(AtributocapaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtributocapaOAALAACCASTContext extends AtributocapaContext {
		public TerminalNode LLAVEIZQUIERDA() { return getToken(ParserG.LLAVEIZQUIERDA, 0); }
		public TerminalNode CAPA() { return getToken(ParserG.CAPA, 0); }
		public List<TerminalNode> DOSPUNTOS() { return getTokens(ParserG.DOSPUNTOS); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(ParserG.DOSPUNTOS, i);
		}
		public List<TerminalNode> TEXTO() { return getTokens(ParserG.TEXTO); }
		public TerminalNode TEXTO(int i) {
			return getToken(ParserG.TEXTO, i);
		}
		public TerminalNode COMA() { return getToken(ParserG.COMA, 0); }
		public TerminalNode ATRIBUTO() { return getToken(ParserG.ATRIBUTO, 0); }
		public TerminalNode LLAVEDERECHA() { return getToken(ParserG.LLAVEDERECHA, 0); }
		public AtributocapaOAALAACCASTContext(AtributocapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitAtributocapaOAALAACCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtributocapaContext atributocapa() throws RecognitionException {
		AtributocapaContext _localctx = new AtributocapaContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_atributocapa);
		try {
			_localctx = new AtributocapaOAALAACCASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			match(LLAVEIZQUIERDA);
			setState(101);
			match(CAPA);
			setState(102);
			match(DOSPUNTOS);
			setState(103);
			match(TEXTO);
			setState(104);
			match(COMA);
			setState(105);
			match(ATRIBUTO);
			setState(106);
			match(DOSPUNTOS);
			setState(107);
			match(TEXTO);
			setState(108);
			match(LLAVEDERECHA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionContext extends ParserRuleContext {
		public ExpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresion; }
	 
		public ExpresionContext() { }
		public void copyFrom(ExpresionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpresionASTContext extends ExpresionContext {
		public TerminalNode LLAVEIZQUIERDA() { return getToken(ParserG.LLAVEIZQUIERDA, 0); }
		public CuerpoexpresionContext cuerpoexpresion() {
			return getRuleContext(CuerpoexpresionContext.class,0);
		}
		public TerminalNode LLAVEDERECHA() { return getToken(ParserG.LLAVEDERECHA, 0); }
		public ExpresionASTContext(ExpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitExpresionAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionContext expresion() throws RecognitionException {
		ExpresionContext _localctx = new ExpresionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_expresion);
		try {
			_localctx = new ExpresionASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(LLAVEIZQUIERDA);
			setState(111);
			cuerpoexpresion();
			setState(112);
			match(LLAVEDERECHA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CuerpoexpresionContext extends ParserRuleContext {
		public CuerpoexpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cuerpoexpresion; }
	 
		public CuerpoexpresionContext() { }
		public void copyFrom(CuerpoexpresionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CuerpoexpresionEASTContext extends CuerpoexpresionContext {
		public TerminalNode OP() { return getToken(ParserG.OP, 0); }
		public List<TerminalNode> DOSPUNTOS() { return getTokens(ParserG.DOSPUNTOS); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(ParserG.DOSPUNTOS, i);
		}
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TerminalNode COMA() { return getToken(ParserG.COMA, 0); }
		public TerminalNode OPERANDOS() { return getToken(ParserG.OPERANDOS, 0); }
		public ListaatributosContext listaatributos() {
			return getRuleContext(ListaatributosContext.class,0);
		}
		public CuerpoexpresionEASTContext(CuerpoexpresionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitCuerpoexpresionEAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CuerpoexpresionContext cuerpoexpresion() throws RecognitionException {
		CuerpoexpresionContext _localctx = new CuerpoexpresionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_cuerpoexpresion);
		try {
			_localctx = new CuerpoexpresionEASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(OP);
			setState(115);
			match(DOSPUNTOS);
			setState(116);
			match(TEXTO);
			setState(117);
			match(COMA);
			setState(118);
			match(OPERANDOS);
			setState(119);
			match(DOSPUNTOS);
			setState(120);
			listaatributos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionlogicaContext extends ParserRuleContext {
		public ExpresionlogicaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionlogica; }
	 
		public ExpresionlogicaContext() { }
		public void copyFrom(ExpresionlogicaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NombrecolumnaELASTContext extends ExpresionlogicaContext {
		public TerminalNode NOMBRECOLUMNA() { return getToken(ParserG.NOMBRECOLUMNA, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public NombrecolumnaELASTContext(ExpresionlogicaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitNombrecolumnaELAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpresionELASTContext extends ExpresionlogicaContext {
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public ExpresionELASTContext(ExpresionlogicaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitExpresionELAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpresionlogicaoplogELASTContext extends ExpresionlogicaContext {
		public ExpresionlogicaoplogContext expresionlogicaoplog() {
			return getRuleContext(ExpresionlogicaoplogContext.class,0);
		}
		public ExpresionlogicaoplogELASTContext(ExpresionlogicaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitExpresionlogicaoplogELAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionlogicaContext expresionlogica() throws RecognitionException {
		ExpresionlogicaContext _localctx = new ExpresionlogicaContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_expresionlogica);
		try {
			setState(127);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				_localctx = new NombrecolumnaELASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(122);
				match(NOMBRECOLUMNA);
				setState(123);
				match(DOSPUNTOS);
				setState(124);
				match(TEXTO);
				}
				}
				break;
			case 2:
				_localctx = new ExpresionELASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(125);
				expresion();
				}
				break;
			case 3:
				_localctx = new ExpresionlogicaoplogELASTContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(126);
				expresionlogicaoplog();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionlogicaoplogContext extends ParserRuleContext {
		public ExpresionlogicaoplogContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionlogicaoplog; }
	 
		public ExpresionlogicaoplogContext() { }
		public void copyFrom(ExpresionlogicaoplogContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExpresionlogicaoplogELOLELASTContext extends ExpresionlogicaoplogContext {
		public TerminalNode LLAVEIZQUIERDA() { return getToken(ParserG.LLAVEIZQUIERDA, 0); }
		public TerminalNode OPLOG() { return getToken(ParserG.OPLOG, 0); }
		public List<TerminalNode> DOSPUNTOS() { return getTokens(ParserG.DOSPUNTOS); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(ParserG.DOSPUNTOS, i);
		}
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TerminalNode COMA() { return getToken(ParserG.COMA, 0); }
		public TerminalNode OPERANDOS() { return getToken(ParserG.OPERANDOS, 0); }
		public ListaatributosContext listaatributos() {
			return getRuleContext(ListaatributosContext.class,0);
		}
		public TerminalNode LLAVEDERECHA() { return getToken(ParserG.LLAVEDERECHA, 0); }
		public ExpresionlogicaoplogELOLELASTContext(ExpresionlogicaoplogContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitExpresionlogicaoplogELOLELAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionlogicaoplogContext expresionlogicaoplog() throws RecognitionException {
		ExpresionlogicaoplogContext _localctx = new ExpresionlogicaoplogContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_expresionlogicaoplog);
		try {
			_localctx = new ExpresionlogicaoplogELOLELASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			match(LLAVEIZQUIERDA);
			setState(130);
			match(OPLOG);
			setState(131);
			match(DOSPUNTOS);
			setState(132);
			match(TEXTO);
			setState(133);
			match(COMA);
			setState(134);
			match(OPERANDOS);
			setState(135);
			match(DOSPUNTOS);
			setState(136);
			listaatributos();
			setState(137);
			match(LLAVEDERECHA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CapasContext extends ParserRuleContext {
		public CapasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_capas; }
	 
		public CapasContext() { }
		public void copyFrom(CapasContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CapasASTContext extends CapasContext {
		public TerminalNode CAPAS() { return getToken(ParserG.CAPAS, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public OpcionescapaContext opcionescapa() {
			return getRuleContext(OpcionescapaContext.class,0);
		}
		public CapasASTContext(CapasContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitCapasAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CapasContext capas() throws RecognitionException {
		CapasContext _localctx = new CapasContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_capas);
		try {
			_localctx = new CapasASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(139);
			match(CAPAS);
			setState(140);
			match(DOSPUNTOS);
			setState(141);
			opcionescapa();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpcionescapaContext extends ParserRuleContext {
		public OpcionescapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opcionescapa; }
	 
		public OpcionescapaContext() { }
		public void copyFrom(OpcionescapaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RepeticioncapaCASTContext extends OpcionescapaContext {
		public TerminalNode CORCHETEIZQUIERDO() { return getToken(ParserG.CORCHETEIZQUIERDO, 0); }
		public RepeticioncapaContext repeticioncapa() {
			return getRuleContext(RepeticioncapaContext.class,0);
		}
		public TerminalNode CORCHETEDERECHO() { return getToken(ParserG.CORCHETEDERECHO, 0); }
		public RepeticioncapaCASTContext(OpcionescapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitRepeticioncapaCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CapavaciaCASTContext extends OpcionescapaContext {
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public CapavaciaCASTContext(OpcionescapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitCapavaciaCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpcionescapaContext opcionescapa() throws RecognitionException {
		OpcionescapaContext _localctx = new OpcionescapaContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_opcionescapa);
		try {
			setState(148);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TEXTO:
				_localctx = new CapavaciaCASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(143);
				match(TEXTO);
				}
				break;
			case CORCHETEIZQUIERDO:
				_localctx = new RepeticioncapaCASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(144);
				match(CORCHETEIZQUIERDO);
				setState(145);
				repeticioncapa();
				setState(146);
				match(CORCHETEDERECHO);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepeticioncapaContext extends ParserRuleContext {
		public RepeticioncapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_repeticioncapa; }
	 
		public RepeticioncapaContext() { }
		public void copyFrom(RepeticioncapaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RepeticioncapaRCCASTContext extends RepeticioncapaContext {
		public List<CapaContext> capa() {
			return getRuleContexts(CapaContext.class);
		}
		public CapaContext capa(int i) {
			return getRuleContext(CapaContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(ParserG.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(ParserG.COMA, i);
		}
		public RepeticioncapaRCCASTContext(RepeticioncapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitRepeticioncapaRCCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RepeticioncapaContext repeticioncapa() throws RecognitionException {
		RepeticioncapaContext _localctx = new RepeticioncapaContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_repeticioncapa);
		int _la;
		try {
			_localctx = new RepeticioncapaRCCASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(150);
			capa();
			setState(155);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMA) {
				{
				{
				setState(151);
				match(COMA);
				setState(152);
				capa();
				}
				}
				setState(157);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CapaContext extends ParserRuleContext {
		public CapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_capa; }
	 
		public CapaContext() { }
		public void copyFrom(CapaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CapaRCRCCASTContext extends CapaContext {
		public TerminalNode LLAVEIZQUIERDA() { return getToken(ParserG.LLAVEIZQUIERDA, 0); }
		public TerminalNode CAPA() { return getToken(ParserG.CAPA, 0); }
		public List<TerminalNode> DOSPUNTOS() { return getTokens(ParserG.DOSPUNTOS); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(ParserG.DOSPUNTOS, i);
		}
		public OpcioncapaContext opcioncapa() {
			return getRuleContext(OpcioncapaContext.class,0);
		}
		public TerminalNode COMA() { return getToken(ParserG.COMA, 0); }
		public TerminalNode RENOMBRE() { return getToken(ParserG.RENOMBRE, 0); }
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TerminalNode LLAVEDERECHA() { return getToken(ParserG.LLAVEDERECHA, 0); }
		public CapaRCRCCASTContext(CapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitCapaRCRCCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CapaContext capa() throws RecognitionException {
		CapaContext _localctx = new CapaContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_capa);
		try {
			_localctx = new CapaRCRCCASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(158);
			match(LLAVEIZQUIERDA);
			setState(159);
			match(CAPA);
			setState(160);
			match(DOSPUNTOS);
			setState(161);
			opcioncapa();
			setState(162);
			match(COMA);
			setState(163);
			match(RENOMBRE);
			setState(164);
			match(DOSPUNTOS);
			setState(165);
			match(TEXTO);
			setState(166);
			match(LLAVEDERECHA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpcioncapaContext extends ParserRuleContext {
		public OpcioncapaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opcioncapa; }
	 
		public OpcioncapaContext() { }
		public void copyFrom(OpcioncapaContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TextoOCCRCRCCASTContext extends OpcioncapaContext {
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TextoOCCRCRCCASTContext(OpcioncapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitTextoOCCRCRCCAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConsultaOCCRCRCCASTContext extends OpcioncapaContext {
		public ConsultaContext consulta() {
			return getRuleContext(ConsultaContext.class,0);
		}
		public ConsultaOCCRCRCCASTContext(OpcioncapaContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitConsultaOCCRCRCCAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpcioncapaContext opcioncapa() throws RecognitionException {
		OpcioncapaContext _localctx = new OpcioncapaContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_opcioncapa);
		try {
			setState(170);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TEXTO:
				_localctx = new TextoOCCRCRCCASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(168);
				match(TEXTO);
				}
				break;
			case LLAVEIZQUIERDA:
				_localctx = new ConsultaOCCRCRCCASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(169);
				consulta();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FiltroContext extends ParserRuleContext {
		public FiltroContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filtro; }
	 
		public FiltroContext() { }
		public void copyFrom(FiltroContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FiltroASTContext extends FiltroContext {
		public TerminalNode FILTRO() { return getToken(ParserG.FILTRO, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public OpcionfiltroContext opcionfiltro() {
			return getRuleContext(OpcionfiltroContext.class,0);
		}
		public FiltroASTContext(FiltroContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitFiltroAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FiltroContext filtro() throws RecognitionException {
		FiltroContext _localctx = new FiltroContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_filtro);
		try {
			_localctx = new FiltroASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(172);
			match(FILTRO);
			setState(173);
			match(DOSPUNTOS);
			setState(174);
			opcionfiltro();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpcionfiltroContext extends ParserRuleContext {
		public OpcionfiltroContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opcionfiltro; }
	 
		public OpcionfiltroContext() { }
		public void copyFrom(OpcionfiltroContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TextoOFFASTContext extends OpcionfiltroContext {
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TextoOFFASTContext(OpcionfiltroContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitTextoOFFAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpresionlogicaOFFASTContext extends OpcionfiltroContext {
		public ExpresionlogicaContext expresionlogica() {
			return getRuleContext(ExpresionlogicaContext.class,0);
		}
		public ExpresionlogicaOFFASTContext(OpcionfiltroContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitExpresionlogicaOFFAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpcionfiltroContext opcionfiltro() throws RecognitionException {
		OpcionfiltroContext _localctx = new OpcionfiltroContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_opcionfiltro);
		try {
			setState(178);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TEXTO:
				_localctx = new TextoOFFASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(176);
				match(TEXTO);
				}
				break;
			case LLAVEIZQUIERDA:
			case NOMBRECOLUMNA:
				_localctx = new ExpresionlogicaOFFASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(177);
				expresionlogica();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpcionesordenamientosagrupamientosContext extends ParserRuleContext {
		public OpcionesordenamientosagrupamientosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opcionesordenamientosagrupamientos; }
	 
		public OpcionesordenamientosagrupamientosContext() { }
		public void copyFrom(OpcionesordenamientosagrupamientosContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AtributosOOAASTContext extends OpcionesordenamientosagrupamientosContext {
		public AtributosContext atributos() {
			return getRuleContext(AtributosContext.class,0);
		}
		public AtributosOOAASTContext(OpcionesordenamientosagrupamientosContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitAtributosOOAAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TextoOOAASTContext extends OpcionesordenamientosagrupamientosContext {
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TextoOOAASTContext(OpcionesordenamientosagrupamientosContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitTextoOOAAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpcionesordenamientosagrupamientosContext opcionesordenamientosagrupamientos() throws RecognitionException {
		OpcionesordenamientosagrupamientosContext _localctx = new OpcionesordenamientosagrupamientosContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_opcionesordenamientosagrupamientos);
		try {
			setState(182);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TEXTO:
				_localctx = new TextoOOAASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(180);
				match(TEXTO);
				}
				break;
			case ATRIBUTOS:
				_localctx = new AtributosOOAASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(181);
				atributos();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrdenamientosContext extends ParserRuleContext {
		public OrdenamientosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordenamientos; }
	 
		public OrdenamientosContext() { }
		public void copyFrom(OrdenamientosContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OrdenamientosASTContext extends OrdenamientosContext {
		public TerminalNode ORDENAMIENTOS() { return getToken(ParserG.ORDENAMIENTOS, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public OpcionesordenamientosagrupamientosContext opcionesordenamientosagrupamientos() {
			return getRuleContext(OpcionesordenamientosagrupamientosContext.class,0);
		}
		public OrdenamientosASTContext(OrdenamientosContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitOrdenamientosAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrdenamientosContext ordenamientos() throws RecognitionException {
		OrdenamientosContext _localctx = new OrdenamientosContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_ordenamientos);
		try {
			_localctx = new OrdenamientosASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			match(ORDENAMIENTOS);
			setState(185);
			match(DOSPUNTOS);
			setState(186);
			opcionesordenamientosagrupamientos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AgrupamientosContext extends ParserRuleContext {
		public AgrupamientosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_agrupamientos; }
	 
		public AgrupamientosContext() { }
		public void copyFrom(AgrupamientosContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AgrupamientosASTContext extends AgrupamientosContext {
		public TerminalNode AGRUPAMIENTOS() { return getToken(ParserG.AGRUPAMIENTOS, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public OpcionesordenamientosagrupamientosContext opcionesordenamientosagrupamientos() {
			return getRuleContext(OpcionesordenamientosagrupamientosContext.class,0);
		}
		public AgrupamientosASTContext(AgrupamientosContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitAgrupamientosAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AgrupamientosContext agrupamientos() throws RecognitionException {
		AgrupamientosContext _localctx = new AgrupamientosContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_agrupamientos);
		try {
			_localctx = new AgrupamientosASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			match(AGRUPAMIENTOS);
			setState(189);
			match(DOSPUNTOS);
			setState(190);
			opcionesordenamientosagrupamientos();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MotorContext extends ParserRuleContext {
		public MotorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_motor; }
	 
		public MotorContext() { }
		public void copyFrom(MotorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MotorASTContext extends MotorContext {
		public TerminalNode MOTOR() { return getToken(ParserG.MOTOR, 0); }
		public TerminalNode DOSPUNTOS() { return getToken(ParserG.DOSPUNTOS, 0); }
		public OpcionesmotorContext opcionesmotor() {
			return getRuleContext(OpcionesmotorContext.class,0);
		}
		public MotorASTContext(MotorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitMotorAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MotorContext motor() throws RecognitionException {
		MotorContext _localctx = new MotorContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_motor);
		try {
			_localctx = new MotorASTContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(MOTOR);
			setState(193);
			match(DOSPUNTOS);
			setState(194);
			opcionesmotor();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpcionesmotorContext extends ParserRuleContext {
		public OpcionesmotorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_opcionesmotor; }
	 
		public OpcionesmotorContext() { }
		public void copyFrom(OpcionesmotorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PostgresOMASTContext extends OpcionesmotorContext {
		public TerminalNode POSTGRES() { return getToken(ParserG.POSTGRES, 0); }
		public PostgresOMASTContext(OpcionesmotorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitPostgresOMAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MongoOMASTContext extends OpcionesmotorContext {
		public TerminalNode MONGO() { return getToken(ParserG.MONGO, 0); }
		public MongoOMASTContext(OpcionesmotorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitMongoOMAST(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TextoOMASTContext extends OpcionesmotorContext {
		public TerminalNode TEXTO() { return getToken(ParserG.TEXTO, 0); }
		public TextoOMASTContext(OpcionesmotorContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserGVisitor ) return ((ParserGVisitor<? extends T>)visitor).visitTextoOMAST(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpcionesmotorContext opcionesmotor() throws RecognitionException {
		OpcionesmotorContext _localctx = new OpcionesmotorContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_opcionesmotor);
		try {
			setState(199);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MONGO:
				_localctx = new MongoOMASTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(196);
				match(MONGO);
				}
				break;
			case POSTGRES:
				_localctx = new PostgresOMASTContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(197);
				match(POSTGRES);
				}
				break;
			case TEXTO:
				_localctx = new TextoOMASTContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(198);
				match(TEXTO);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\"\u00cc\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\7\6N\n\6\f\6\16\6Q\13\6\3\6\3\6"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\5\be"+
		"\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\5\f\u0082\n\f\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17"+
		"\3\17\3\17\5\17\u0097\n\17\3\20\3\20\3\20\7\20\u009c\n\20\f\20\16\20\u009f"+
		"\13\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\5\22"+
		"\u00ad\n\22\3\23\3\23\3\23\3\23\3\24\3\24\5\24\u00b5\n\24\3\25\3\25\5"+
		"\25\u00b9\n\25\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\30\3\30\3\30"+
		"\3\30\3\31\3\31\3\31\5\31\u00ca\n\31\3\31\2\2\32\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\2\2\2\u00c2\2\62\3\2\2\2\4\65\3\2\2\2\6"+
		"9\3\2\2\2\bE\3\2\2\2\nI\3\2\2\2\fT\3\2\2\2\16d\3\2\2\2\20f\3\2\2\2\22"+
		"p\3\2\2\2\24t\3\2\2\2\26\u0081\3\2\2\2\30\u0083\3\2\2\2\32\u008d\3\2\2"+
		"\2\34\u0096\3\2\2\2\36\u0098\3\2\2\2 \u00a0\3\2\2\2\"\u00ac\3\2\2\2$\u00ae"+
		"\3\2\2\2&\u00b4\3\2\2\2(\u00b8\3\2\2\2*\u00ba\3\2\2\2,\u00be\3\2\2\2."+
		"\u00c2\3\2\2\2\60\u00c9\3\2\2\2\62\63\5\4\3\2\63\64\7\2\2\3\64\3\3\2\2"+
		"\2\65\66\7\4\2\2\66\67\5\6\4\2\678\7\5\2\28\5\3\2\2\29:\5\b\5\2:;\7\6"+
		"\2\2;<\5\32\16\2<=\7\6\2\2=>\5$\23\2>?\7\6\2\2?@\5*\26\2@A\7\6\2\2AB\5"+
		",\27\2BC\7\6\2\2CD\5.\30\2D\7\3\2\2\2EF\7\21\2\2FG\7\3\2\2GH\5\n\6\2H"+
		"\t\3\2\2\2IJ\7\7\2\2JO\5\f\7\2KL\7\6\2\2LN\5\f\7\2MK\3\2\2\2NQ\3\2\2\2"+
		"OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2\2RS\7\b\2\2S\13\3\2\2\2TU\7\4\2"+
		"\2UV\7\22\2\2VW\7\3\2\2WX\5\16\b\2XY\7\6\2\2YZ\7\30\2\2Z[\7\3\2\2[\\\7"+
		"!\2\2\\]\7\5\2\2]\r\3\2\2\2^e\7!\2\2_e\7 \2\2`e\5\20\t\2ae\5\4\3\2be\5"+
		"\24\13\2ce\5\26\f\2d^\3\2\2\2d_\3\2\2\2d`\3\2\2\2da\3\2\2\2db\3\2\2\2"+
		"dc\3\2\2\2e\17\3\2\2\2fg\7\4\2\2gh\7\24\2\2hi\7\3\2\2ij\7!\2\2jk\7\6\2"+
		"\2kl\7\22\2\2lm\7\3\2\2mn\7!\2\2no\7\5\2\2o\21\3\2\2\2pq\7\4\2\2qr\5\24"+
		"\13\2rs\7\5\2\2s\23\3\2\2\2tu\7\31\2\2uv\7\3\2\2vw\7!\2\2wx\7\6\2\2xy"+
		"\7\32\2\2yz\7\3\2\2z{\5\n\6\2{\25\3\2\2\2|}\7\34\2\2}~\7\3\2\2~\u0082"+
		"\7!\2\2\177\u0082\5\22\n\2\u0080\u0082\5\30\r\2\u0081|\3\2\2\2\u0081\177"+
		"\3\2\2\2\u0081\u0080\3\2\2\2\u0082\27\3\2\2\2\u0083\u0084\7\4\2\2\u0084"+
		"\u0085\7\33\2\2\u0085\u0086\7\3\2\2\u0086\u0087\7!\2\2\u0087\u0088\7\6"+
		"\2\2\u0088\u0089\7\32\2\2\u0089\u008a\7\3\2\2\u008a\u008b\5\n\6\2\u008b"+
		"\u008c\7\5\2\2\u008c\31\3\2\2\2\u008d\u008e\7\23\2\2\u008e\u008f\7\3\2"+
		"\2\u008f\u0090\5\34\17\2\u0090\33\3\2\2\2\u0091\u0097\7!\2\2\u0092\u0093"+
		"\7\7\2\2\u0093\u0094\5\36\20\2\u0094\u0095\7\b\2\2\u0095\u0097\3\2\2\2"+
		"\u0096\u0091\3\2\2\2\u0096\u0092\3\2\2\2\u0097\35\3\2\2\2\u0098\u009d"+
		"\5 \21\2\u0099\u009a\7\6\2\2\u009a\u009c\5 \21\2\u009b\u0099\3\2\2\2\u009c"+
		"\u009f\3\2\2\2\u009d\u009b\3\2\2\2\u009d\u009e\3\2\2\2\u009e\37\3\2\2"+
		"\2\u009f\u009d\3\2\2\2\u00a0\u00a1\7\4\2\2\u00a1\u00a2\7\24\2\2\u00a2"+
		"\u00a3\7\3\2\2\u00a3\u00a4\5\"\22\2\u00a4\u00a5\7\6\2\2\u00a5\u00a6\7"+
		"\30\2\2\u00a6\u00a7\7\3\2\2\u00a7\u00a8\7!\2\2\u00a8\u00a9\7\5\2\2\u00a9"+
		"!\3\2\2\2\u00aa\u00ad\7!\2\2\u00ab\u00ad\5\4\3\2\u00ac\u00aa\3\2\2\2\u00ac"+
		"\u00ab\3\2\2\2\u00ad#\3\2\2\2\u00ae\u00af\7\25\2\2\u00af\u00b0\7\3\2\2"+
		"\u00b0\u00b1\5&\24\2\u00b1%\3\2\2\2\u00b2\u00b5\7!\2\2\u00b3\u00b5\5\26"+
		"\f\2\u00b4\u00b2\3\2\2\2\u00b4\u00b3\3\2\2\2\u00b5\'\3\2\2\2\u00b6\u00b9"+
		"\7!\2\2\u00b7\u00b9\5\b\5\2\u00b8\u00b6\3\2\2\2\u00b8\u00b7\3\2\2\2\u00b9"+
		")\3\2\2\2\u00ba\u00bb\7\26\2\2\u00bb\u00bc\7\3\2\2\u00bc\u00bd\5(\25\2"+
		"\u00bd+\3\2\2\2\u00be\u00bf\7\27\2\2\u00bf\u00c0\7\3\2\2\u00c0\u00c1\5"+
		"(\25\2\u00c1-\3\2\2\2\u00c2\u00c3\7\35\2\2\u00c3\u00c4\7\3\2\2\u00c4\u00c5"+
		"\5\60\31\2\u00c5/\3\2\2\2\u00c6\u00ca\7\36\2\2\u00c7\u00ca\7\37\2\2\u00c8"+
		"\u00ca\7!\2\2\u00c9\u00c6\3\2\2\2\u00c9\u00c7\3\2\2\2\u00c9\u00c8\3\2"+
		"\2\2\u00ca\61\3\2\2\2\13Od\u0081\u0096\u009d\u00ac\u00b4\u00b8\u00c9";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}