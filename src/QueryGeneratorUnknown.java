import generated.ParserG;
import generated.ParserGVisitor;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.LinkedList;

public class QueryGeneratorUnknown implements ParserGVisitor {
    LinkedList<String> postgisFunctions;

    public QueryGeneratorUnknown(){
        LinkedList<String> postgisFunctions = new LinkedList<>();
        this.llenarLista();
    }

    private void llenarLista(){
        postgisFunctions.add("ST_3DExtent");
        postgisFunctions.add("ST_Accum");
        postgisFunctions.add("ST_AsGeobuf");
        postgisFunctions.add("ST_AsMVT");
        postgisFunctions.add("ST_ClusterIntersecting");
        postgisFunctions.add("ST_ClusterWithin");
        postgisFunctions.add("ST_Collect");
        postgisFunctions.add("ST_Extent");
        postgisFunctions.add("ST_MakeLine");
        postgisFunctions.add("ST_MemUnion");
        postgisFunctions.add("ST_Polygonize");
        postgisFunctions.add("ST_SameAlignment");
        postgisFunctions.add("ST_Union");
        postgisFunctions.add("ST_ClusterDBSCAN");
        postgisFunctions.add("ST_ClusterKMeans");
        postgisFunctions.add("ST_3DDWithin");
        postgisFunctions.add("ST_3DDistance");
        postgisFunctions.add("ST_3DIntersects");
        postgisFunctions.add("ST_AddEdgeModFace");
        postgisFunctions.add("ST_AddEdgeNewFaces");
        postgisFunctions.add("ST_AddIsoEdge");
        postgisFunctions.add("ST_AddIsoNode");
        postgisFunctions.add("ST_Area");
        postgisFunctions.add("ST_AsBinary");
        postgisFunctions.add("ST_AsText");
        postgisFunctions.add("ST_Boundary");
        postgisFunctions.add("ST_Buffer");
        postgisFunctions.add("ST_Centroid");
        postgisFunctions.add("ST_ChangeEdgeGeom");
        postgisFunctions.add("ST_Contains");
        postgisFunctions.add("ST_ConvexHull");
        postgisFunctions.add("ST_CoordDim");
        postgisFunctions.add("ST_CreateTopoGeo");
        postgisFunctions.add("ST_Crosses");
        postgisFunctions.add("ST_CurveToLine");
        postgisFunctions.add("ST_Difference");
        postgisFunctions.add("ST_Dimension");
        postgisFunctions.add("ST_Disjoint");
        postgisFunctions.add("ST_Distance");
        postgisFunctions.add("ST_EndPoint");
        postgisFunctions.add("ST_Envelope");
        postgisFunctions.add("ST_Equals");
        postgisFunctions.add("ST_ExteriorRing");
        postgisFunctions.add("ST_GMLToSQL");
        postgisFunctions.add("ST_GeomCollFromText");
        postgisFunctions.add("ST_GeomFromText");
        postgisFunctions.add("ST_GeomFromWKB");
        postgisFunctions.add("ST_GeometryFromText");
        postgisFunctions.add("ST_GeometryN");
        postgisFunctions.add("ST_GeometryType");
        postgisFunctions.add("ST_GetFaceEdges");
        postgisFunctions.add("ST_GetFaceGeometry");
        postgisFunctions.add("ST_InitTopoGeo");
        postgisFunctions.add("ST_InteriorRingN");
        postgisFunctions.add("ST_Intersection");
        postgisFunctions.add("ST_Intersects");
        postgisFunctions.add("ST_IsClosed");
        postgisFunctions.add("ST_IsEmpty");
        postgisFunctions.add("ST_IsRing");
        postgisFunctions.add("ST_IsSimple");
        postgisFunctions.add("ST_IsValid");
        postgisFunctions.add("ST_Length");
        postgisFunctions.add("ST_LineFromText");
        postgisFunctions.add("ST_LineFromWKB");
        postgisFunctions.add("ST_LinestringFromWKB");
        postgisFunctions.add("ST_M");
        postgisFunctions.add("ST_MLineFromText");
        postgisFunctions.add("ST_MPointFromText");
        postgisFunctions.add("ST_MPolyFromText");
        postgisFunctions.add("ST_ModEdgeHeal");
        postgisFunctions.add("ST_ModEdgeSplit");
        postgisFunctions.add("ST_MoveIsoNode");
        postgisFunctions.add("ST_NewEdgeHeal");
        postgisFunctions.add("ST_NewEdgesSplit");
        postgisFunctions.add("ST_NumGeometries");
        postgisFunctions.add("ST_NumInteriorRings");
        postgisFunctions.add("ST_NumPatches");
        postgisFunctions.add("ST_NumPoints");
        postgisFunctions.add("ST_OrderingEquals");
        postgisFunctions.add("ST_Overlaps");
        postgisFunctions.add("ST_PatchN");
        postgisFunctions.add("ST_Perimeter");
        postgisFunctions.add("ST_Point");
        postgisFunctions.add("ST_PointFromText");
        postgisFunctions.add("ST_PointFromWKB");
        postgisFunctions.add("ST_PointN");
        postgisFunctions.add("ST_PointOnSurface");
        postgisFunctions.add("ST_Polygon");
        postgisFunctions.add("ST_PolygonFromText");
        postgisFunctions.add("ST_Relate");
        postgisFunctions.add("ST_RemEdgeModFace");
        postgisFunctions.add("ST_RemEdgeNewFace");
        postgisFunctions.add("ST_RemoveIsoEdge");
        postgisFunctions.add("ST_RemoveIsoNode");
        postgisFunctions.add("ST_SRID");
        postgisFunctions.add("ST_StartPoint");
        postgisFunctions.add("ST_SymDifference");
        postgisFunctions.add("ST_Touches");
        postgisFunctions.add("ST_Transform");
        postgisFunctions.add("ST_WKBToSQL");
        postgisFunctions.add("ST_WKTToSQL");
        postgisFunctions.add("ST_Within");
        postgisFunctions.add("ST_X");
        postgisFunctions.add("ST_Y");
        postgisFunctions.add("ST_Z");
        postgisFunctions.add("ST_AsEWKT");
        postgisFunctions.add("ST_AsGML");
        postgisFunctions.add("ST_AsGeoJSON");
        postgisFunctions.add("ST_AsKML");
        postgisFunctions.add("ST_AsSVG");
        postgisFunctions.add("ST_Azimuth");
        postgisFunctions.add("ST_CoveredBy");
        postgisFunctions.add("ST_Covers");
        postgisFunctions.add("ST_DWithin");
        postgisFunctions.add("ST_GeogFromText");
        postgisFunctions.add("ST_GeogFromWKB");
        postgisFunctions.add("ST_GeographyFromText");
        postgisFunctions.add("ST_Project");
        postgisFunctions.add("ST_Segmentize");
        postgisFunctions.add("ST_Summary");
        postgisFunctions.add("ST_Retile");
        postgisFunctions.add("ST_AddBand");
        postgisFunctions.add("ST_AsGDALRaster");
        postgisFunctions.add("ST_AsJPEG");
        postgisFunctions.add("ST_AsPNG");
        postgisFunctions.add("ST_AsRaster");
        postgisFunctions.add("ST_AsTIFF");
        postgisFunctions.add("ST_Aspect");
        postgisFunctions.add("ST_Band");
        postgisFunctions.add("ST_BandIsNoData");
        postgisFunctions.add("ST_BandMetaData");
        postgisFunctions.add("ST_BandNoDataValue");
        postgisFunctions.add("ST_BandPath");
        postgisFunctions.add("ST_BandPixelType");
        postgisFunctions.add("ST_Clip");
        postgisFunctions.add("ST_ColorMap");
        postgisFunctions.add("ST_ContainsProperly");
        postgisFunctions.add("ST_Count");
        postgisFunctions.add("ST_CountAgg");
        postgisFunctions.add("ST_DFullyWithin");
        postgisFunctions.add("ST_DumpAsPolygons");
        postgisFunctions.add("ST_DumpValues");
        postgisFunctions.add("ST_FromGDALRaster");
        postgisFunctions.add("ST_GeoReference");
        postgisFunctions.add("ST_HasNoBand");
        postgisFunctions.add("ST_Height");
        postgisFunctions.add("ST_HillShade");
        postgisFunctions.add("ST_Histogram");
        postgisFunctions.add("ST_MakeEmptyCoverage");
        postgisFunctions.add("ST_MakeEmptyRaster");
        postgisFunctions.add("ST_MapAlgebra");
        postgisFunctions.add("ST_MapAlgebraExpr");
        postgisFunctions.add("ST_MapAlgebraFct");
        postgisFunctions.add("ST_MapAlgebraFctNgb");
        postgisFunctions.add("ST_MemSize");
        postgisFunctions.add("ST_MetaData");
        postgisFunctions.add("ST_MinConvexHull");
        postgisFunctions.add("ST_NearestValue");
        postgisFunctions.add("ST_Neighborhood");
        postgisFunctions.add("ST_NotSameAlignmentReason");
        postgisFunctions.add("ST_NumBands");
        postgisFunctions.add("ST_PixelAsCentroid");
        postgisFunctions.add("ST_PixelAsCentroids");
        postgisFunctions.add("ST_PixelAsPoint");
        postgisFunctions.add("ST_PixelAsPoints");
        postgisFunctions.add("ST_PixelAsPolygon");
        postgisFunctions.add("ST_PixelAsPolygons");
        postgisFunctions.add("ST_PixelHeight");
        postgisFunctions.add("ST_PixelOfValue");
        postgisFunctions.add("ST_PixelWidth");
        postgisFunctions.add("ST_Quantile");
        postgisFunctions.add("ST_RasterToWorldCoord");
        postgisFunctions.add("ST_RasterToWorldCoordX");
        postgisFunctions.add("ST_RasterToWorldCoordY");
        postgisFunctions.add("ST_Reclass");
        postgisFunctions.add("ST_Resample");
        postgisFunctions.add("ST_Rescale");
        postgisFunctions.add("ST_Resize");
        postgisFunctions.add("ST_Reskew");
        postgisFunctions.add("ST_Rotation");
        postgisFunctions.add("ST_Roughness");
        postgisFunctions.add("ST_ScaleX");
        postgisFunctions.add("ST_ScaleY");
        postgisFunctions.add("ST_SetBandIsNoData");
        postgisFunctions.add("ST_SetBandNoDataValue");
        postgisFunctions.add("ST_SetGeoReference");
        postgisFunctions.add("ST_SetRotation");
        postgisFunctions.add("ST_SetSRID");
        postgisFunctions.add("ST_SetScale");
        postgisFunctions.add("ST_SetSkew");
        postgisFunctions.add("ST_SetUpperLeft");
        postgisFunctions.add("ST_SetValue");
        postgisFunctions.add("ST_SetValues");
        postgisFunctions.add("ST_SkewX");
        postgisFunctions.add("ST_SkewY");
        postgisFunctions.add("ST_Slope");
        postgisFunctions.add("ST_SnapToGrid");
        postgisFunctions.add("ST_SummaryStats");
        postgisFunctions.add("ST_SummaryStatsAgg");
        postgisFunctions.add("ST_TPI");
        postgisFunctions.add("ST_TRI");
        postgisFunctions.add("ST_Tile");
        postgisFunctions.add("ST_UpperLeftX");
        postgisFunctions.add("ST_UpperLeftY");
        postgisFunctions.add("ST_Value");
        postgisFunctions.add("ST_ValueCount");
        postgisFunctions.add("ST_Width");
        postgisFunctions.add("ST_WorldToRasterCoord");
        postgisFunctions.add("ST_WorldToRasterCoordX");
        postgisFunctions.add("ST_WorldToRasterCoordY");
        postgisFunctions.add("ST_Dump");
        postgisFunctions.add("ST_DumpPoints");
        postgisFunctions.add("ST_DumpRings");
        postgisFunctions.add("ST_3DMakeBox");
        postgisFunctions.add("ST_AsMVTGeom");
        postgisFunctions.add("ST_AsTWKB");
        postgisFunctions.add("ST_Box2dFromGeoHash");
        postgisFunctions.add("ST_ClipByBox2D");
        postgisFunctions.add("ST_EstimatedExtent");
        postgisFunctions.add("ST_Expand");
        postgisFunctions.add("ST_MakeBox2D");
        postgisFunctions.add("ST_XMax");
        postgisFunctions.add("ST_XMin");
        postgisFunctions.add("ST_YMax");
        postgisFunctions.add("ST_YMin");
        postgisFunctions.add("ST_ZMax");
        postgisFunctions.add("ST_ZMin");
        postgisFunctions.add("ST_3DArea");
        postgisFunctions.add("ST_3DClosestPoint");
        postgisFunctions.add("ST_3DDFullyWithin");
        postgisFunctions.add("ST_3DDifference");
        postgisFunctions.add("ST_3DIntersection");
        postgisFunctions.add("ST_3DLength");
        postgisFunctions.add("ST_3DLongestLine");
        postgisFunctions.add("ST_3DMaxDistance");
        postgisFunctions.add("ST_3DPerimeter");
        postgisFunctions.add("ST_3DShortestLine");
        postgisFunctions.add("ST_3DUnion");
        postgisFunctions.add("ST_AddMeasure");
        postgisFunctions.add("ST_AddPoint");
        postgisFunctions.add("ST_Affine");
        postgisFunctions.add("ST_ApproximateMedialAxis");
        postgisFunctions.add("ST_AsEWKB");
        postgisFunctions.add("ST_AsHEXEWKB");
        postgisFunctions.add("ST_AsX3D");
        postgisFunctions.add("ST_BoundingDiagonal");
        postgisFunctions.add("ST_CPAWithin");
        postgisFunctions.add("ST_ClosestPointOfApproach");
        postgisFunctions.add("ST_DelaunayTriangles");
        postgisFunctions.add("ST_DistanceCPA");
        postgisFunctions.add("ST_Extrude");
        postgisFunctions.add("ST_FlipCoordinates");
        postgisFunctions.add("ST_Force2D");
        postgisFunctions.add("ST_ForceCurve");
        postgisFunctions.add("ST_ForceLHR");
        postgisFunctions.add("ST_ForcePolygonCCW");
        postgisFunctions.add("ST_ForcePolygonCW");
        postgisFunctions.add("ST_ForceRHR");
        postgisFunctions.add("ST_ForceSFS");
        postgisFunctions.add("ST_Force3D");
        postgisFunctions.add("ST_Force3DZ");
        postgisFunctions.add("ST_Force4D");
        postgisFunctions.add("ST_ForceCollection");
        postgisFunctions.add("ST_GeomFromEWKB");
        postgisFunctions.add("ST_GeomFromEWKT");
        postgisFunctions.add("ST_GeomFromGML");
        postgisFunctions.add("ST_GeomFromGeoJSON");
        postgisFunctions.add("ST_GeomFromKML");
        postgisFunctions.add("ST_GeometricMedian");
        postgisFunctions.add("ST_HasArc");
        postgisFunctions.add("ST_InterpolatePoint");
        postgisFunctions.add("ST_IsCollection");
        postgisFunctions.add("ST_IsPlanar");
        postgisFunctions.add("ST_IsPolygonCCW");
        postgisFunctions.add("ST_IsPolygonCW");
        postgisFunctions.add("ST_IsSolid");
        postgisFunctions.add("ST_IsValidTrajectory");
        postgisFunctions.add("ST_LengthSpheroid");
        postgisFunctions.add("ST_LineFromMultiPoint");
        postgisFunctions.add("ST_LineInterpolatePoint");
        postgisFunctions.add("ST_LineSubstring");
        postgisFunctions.add("ST_LineToCurve");
        postgisFunctions.add("ST_LocateBetweenElevations");
        postgisFunctions.add("ST_MakePoint");
        postgisFunctions.add("ST_MakePolygon");
        postgisFunctions.add("ST_MakeSolid");
        postgisFunctions.add("ST_MakeValid");
        postgisFunctions.add("ST_NDims");
        postgisFunctions.add("ST_NPoints");
        postgisFunctions.add("ST_NRings");
        postgisFunctions.add("ST_Node");
        postgisFunctions.add("ST_Orientation");
        postgisFunctions.add("ST_Points");
        postgisFunctions.add("ST_RemovePoint");
        postgisFunctions.add("ST_RemoveRepeatedPoints");
        postgisFunctions.add("ST_Reverse");
        postgisFunctions.add("ST_Rotate");
        postgisFunctions.add("ST_RotateX");
        postgisFunctions.add("ST_RotateY");
        postgisFunctions.add("ST_RotateZ");
        postgisFunctions.add("ST_Scale");
        postgisFunctions.add("ST_SetPoint");
        postgisFunctions.add("ST_ShiftLongitude");
        postgisFunctions.add("ST_StraightSkeleton");
        postgisFunctions.add("ST_SwapOrdinates");
        postgisFunctions.add("ST_Tesselate");
        postgisFunctions.add("ST_TransScale");
        postgisFunctions.add("ST_Translate");
        postgisFunctions.add("ST_UnaryUnion");
        postgisFunctions.add("ST_Volume");
        postgisFunctions.add("ST_WrapX");
        postgisFunctions.add("ST_Zmflag");
        postgisFunctions.add("ST_Force3DM");
        postgisFunctions.add("ST_GeoHash");
        postgisFunctions.add("ST_AsEncodedPolyline");
        postgisFunctions.add("ST_AsLatLonText");
        postgisFunctions.add("ST_BdMPolyFromText");
        postgisFunctions.add("ST_BdPolyFromText");
        postgisFunctions.add("ST_BuildArea");
        postgisFunctions.add("ST_ClosestPoint");
        postgisFunctions.add("ST_CollectionExtract");
        postgisFunctions.add("ST_CollectionHomogenize");
        postgisFunctions.add("ST_ConcaveHull");
        postgisFunctions.add("ST_DistanceSphere");
        postgisFunctions.add("ST_DistanceSpheroid");
        postgisFunctions.add("ST_FrechetDistance");
        postgisFunctions.add("ST_GeneratePoints");
        postgisFunctions.add("ST_GeomFromGeoHash");
        postgisFunctions.add("ST_GeomFromTWKB");
        postgisFunctions.add("ST_HausdorffDistance");
        postgisFunctions.add("ST_IsValidDetail");
        postgisFunctions.add("ST_IsValidReason");
        postgisFunctions.add("ST_Length2D");
        postgisFunctions.add("ST_Length2D_Spheroid");
        postgisFunctions.add("ST_LineCrossingDirection");
        postgisFunctions.add("ST_LineFromEncodedPolyline");
        postgisFunctions.add("ST_LineLocatePoint");
        postgisFunctions.add("ST_LineMerge");
        postgisFunctions.add("ST_LocateAlong");
        postgisFunctions.add("ST_LocateBetween");
        postgisFunctions.add("ST_LongestLine");
        postgisFunctions.add("ST_MakeEnvelope");
        postgisFunctions.add("ST_MakePointM");
        postgisFunctions.add("ST_MaxDistance");
        postgisFunctions.add("ST_MinimumBoundingCircle");
        postgisFunctions.add("ST_MinimumBoundingRadius");
        postgisFunctions.add("ST_MinimumClearance");
        postgisFunctions.add("ST_MinimumClearanceLine");
        postgisFunctions.add("ST_MinkowskiSum");
        postgisFunctions.add("ST_Multi");
        postgisFunctions.add("ST_Normalize");
        postgisFunctions.add("ST_NumInteriorRing");
        postgisFunctions.add("ST_OffsetCurve");
        postgisFunctions.add("ST_Perimeter2D");
        postgisFunctions.add("ST_PointFromGeoHash");
        postgisFunctions.add("ST_PointInsideCircle");
        postgisFunctions.add("ST_RelateMatch");
        postgisFunctions.add("ST_SetEffectiveArea");
        postgisFunctions.add("ST_SharedPaths");
        postgisFunctions.add("ST_ShortestLine");
        postgisFunctions.add("ST_Simplify");
        postgisFunctions.add("ST_SimplifyPreserveTopology");
        postgisFunctions.add("ST_SimplifyVW");
        postgisFunctions.add("ST_Snap");
        postgisFunctions.add("ST_Split");
        postgisFunctions.add("ST_Subdivide");
        postgisFunctions.add("ST_VoronoiLines");
        postgisFunctions.add("ST_VoronoiPolygons");
        postgisFunctions.add("ST_CreateOverview");
        postgisFunctions.add("ST_InvDistWeight4ma");
        postgisFunctions.add("ST_MinDist4ma");
        postgisFunctions.add("ST_Distinct4ma");
        postgisFunctions.add("ST_Max4ma");
        postgisFunctions.add("ST_Mean4ma");
        postgisFunctions.add("ST_Min4ma");
        postgisFunctions.add("ST_Range4ma");
        postgisFunctions.add("ST_StdDev4ma");
        postgisFunctions.add("ST_Sum4ma");
        postgisFunctions.add("ST_GDALDrivers");
    }

    private boolean esFuncionPostgres(String func){
        return postgisFunctions.contains(func);
    }

    @Override
    public Object visitProgramAST(ParserG.ProgramASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaAST(ParserG.ConsultaASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoconsultaAST(ParserG.CuerpoconsultaASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributosCCAST(ParserG.AtributosCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitListaatributosACCAST(ParserG.ListaatributosACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributoLAACCAST(ParserG.AtributoLAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoALAACCAST(ParserG.TextoALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitNumeroALAACCAST(ParserG.NumeroALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributocapaALAACCAST(ParserG.AtributocapaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaALAACCAST(ParserG.ConsultaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoexpresionALAACCAST(ParserG.CuerpoexpresionALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaALAACCAST(ParserG.ExpresionlogicaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributocapaOAALAACCAST(ParserG.AtributocapaOAALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionAST(ParserG.ExpresionASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoexpresionEAST(ParserG.CuerpoexpresionEASTContext ctx) {
        return null;
    }

    @Override
    public Object visitNombrecolumnaELAST(ParserG.NombrecolumnaELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionELAST(ParserG.ExpresionELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaoplogELAST(ParserG.ExpresionlogicaoplogELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaoplogELOLELAST(ParserG.ExpresionlogicaoplogELOLELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapasAST(ParserG.CapasASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapavaciaCAST(ParserG.CapavaciaCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitRepeticioncapaCAST(ParserG.RepeticioncapaCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitRepeticioncapaRCCAST(ParserG.RepeticioncapaRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapaRCRCCAST(ParserG.CapaRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOCCRCRCCAST(ParserG.TextoOCCRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaOCCRCRCCAST(ParserG.ConsultaOCCRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitFiltroAST(ParserG.FiltroASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOFFAST(ParserG.TextoOFFASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaOFFAST(ParserG.ExpresionlogicaOFFASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOOAAST(ParserG.TextoOOAASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributosOOAAST(ParserG.AtributosOOAASTContext ctx) {
        return null;
    }

    @Override
    public Object visitOrdenamientosAST(ParserG.OrdenamientosASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAgrupamientosAST(ParserG.AgrupamientosASTContext ctx) {
        return null;
    }

    @Override
    public Object visitMotorAST(ParserG.MotorASTContext ctx) {
        return null;
    }

    @Override
    public Object visitMongoOMAST(ParserG.MongoOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visitPostgresOMAST(ParserG.PostgresOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOMAST(ParserG.TextoOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visit(ParseTree parseTree) {
        return null;
    }

    @Override
    public Object visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public Object visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    @Override
    public Object visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
