lexer grammar Scanner;

//Símbolos:
DOSPUNTOS           : ':' ;
LLAVEIZQUIERDA      : '{' ;
LLAVEDERECHA        : '}' ;
COMA                : ',' ;
CORCHETEIZQUIERDO   : '[' ;
CORCHETEDERECHO     : ']' ;


//Operadores lógicos:

/*
IGUAL       : '"=="'   ;
DIFERENTE   : '"!="'  ;
MAYOR       : '">"'   ;
MAYOROIGUAL : '">="'  ;
MENOR       : '"<"'   ;
MENOROIGUAL : '"<="'  ;
NOT         : '"!"'   ;
AND         : '"&"'   ;
OR          : '"|"'   ;*/

//Funciones:
BUFFER      : 'buffer'      ;
INTERSECT   : 'interserct'  ;
NEAR        : 'near'        ;
DISTANCE    : 'distance'    ;
DISTINCT    : 'distinct'    ;
WITHIN      : 'within'      ;
UPPER       : 'upper'       ;

//Operadores matemáticos:
/*SUMA            : '"+"' ;
RESTA           : '"-"' ;
MULTIPLICACION  : '"*"' ;
DIVISION        : '"/"' ;
RESIDUO         : '"%"' ;*/

//Operación concatenar
CONCATENAR      : 'concatenar' ;

//Palabras reservadas
ATRIBUTOS       : 'atributos'       ;
ATRIBUTO        : 'atributo'        ;
CAPAS           : 'capas'           ;
CAPA            : 'capa'            ;
FILTRO          : 'filtro'          ;
ORDENAMIENTOS   : 'ordenamientos'   ;
AGRUPAMIENTOS   : 'agrupamientos'   ;
RENOMBRE        : 'renombre'        ;
OP              : 'op'              ;
OPERANDOS       : 'operandos'       ;
OPLOG           : 'opLog'           ;
NOMBRECOLUMNA   : 'nombreColumna'   ;
MOTOR           : 'motor'           ;
MONGO           : 'Mongo'           ;
POSTGRES        : 'PostgreSQL'      ;


NUMERO : DIGITO DIGITO* ;
TEXTO : '"' ('""'|~'"')* '"';


//fragment COMILLAS            : '"' ;
fragment DIGITO : '0'..'9';

ESPACIOENBLANCO  :   [ \t\n\r]+ -> skip ;
