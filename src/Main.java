import generated.ParserG;
import generated.Scanner;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.List;

public class Main {
    public static void main(String[] args){
        Scanner inst = null;
        ParserG parser = null;
        ParseTree tree;
        QueryGeneratorPostgres qgPostgres;

        CharStream input=null;
        CommonTokenStream tokens = null;
        try {
            input = CharStreams.fromFileName("test.txt");
            inst = new Scanner(input);
            tokens = new CommonTokenStream(inst);
            parser = new ParserG(tokens);
            tree = parser.program();
            //EngineIdentifier ei = new EngineIdentifier();
           // String motor = (String) ei.visit(tree);
            //System.out.println("Motor: " + motor);

            QueryGeneratorPostgres qg = new QueryGeneratorPostgres();
            System.out.println((String) qg.visit(tree));
            //hacer el visit sólo al nodo de motor para ver con cual árbol se procede
        }
        catch(Exception e){
            System.out.println("No hay archivo " + e.toString());
            e.printStackTrace();
        }
        inst.reset();
        List<Token> lista = (List<Token>) inst.getAllTokens();

    }
}
