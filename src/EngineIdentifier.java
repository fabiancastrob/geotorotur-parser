import generated.ParserG;
import generated.ParserGBaseVisitor;

public class EngineIdentifier extends ParserGBaseVisitor {
    public EngineIdentifier() {

    }

    @Override public Object visitProgramAST(ParserG.ProgramASTContext ctx) {
        return visit(ctx.consulta());
    }


    @Override public Object visitConsultaAST(ParserG.ConsultaASTContext ctx) {
        return visit(ctx.cuerpoconsulta());
    }


    @Override public Object visitCuerpoconsultaAST(ParserG.CuerpoconsultaASTContext ctx) {
        return visit(ctx.motor());
    }


    @Override public Object visitMotorAST(ParserG.MotorASTContext ctx) {
        return ctx.opcionesmotor().getText();
    }

}
