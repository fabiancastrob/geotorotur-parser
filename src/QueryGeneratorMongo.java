import generated.ParserG;
import generated.ParserGVisitor;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class QueryGeneratorMongo implements ParserGVisitor {
    @Override
    public Object visitProgramAST(ParserG.ProgramASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaAST(ParserG.ConsultaASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoconsultaAST(ParserG.CuerpoconsultaASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributosCCAST(ParserG.AtributosCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitListaatributosACCAST(ParserG.ListaatributosACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributoLAACCAST(ParserG.AtributoLAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoALAACCAST(ParserG.TextoALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitNumeroALAACCAST(ParserG.NumeroALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributocapaALAACCAST(ParserG.AtributocapaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaALAACCAST(ParserG.ConsultaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoexpresionALAACCAST(ParserG.CuerpoexpresionALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaALAACCAST(ParserG.ExpresionlogicaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributocapaOAALAACCAST(ParserG.AtributocapaOAALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionAST(ParserG.ExpresionASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoexpresionEAST(ParserG.CuerpoexpresionEASTContext ctx) {
        return null;
    }

    @Override
    public Object visitNombrecolumnaELAST(ParserG.NombrecolumnaELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionELAST(ParserG.ExpresionELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaoplogELAST(ParserG.ExpresionlogicaoplogELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaoplogELOLELAST(ParserG.ExpresionlogicaoplogELOLELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapasAST(ParserG.CapasASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapavaciaCAST(ParserG.CapavaciaCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitRepeticioncapaCAST(ParserG.RepeticioncapaCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitRepeticioncapaRCCAST(ParserG.RepeticioncapaRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapaRCRCCAST(ParserG.CapaRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOCCRCRCCAST(ParserG.TextoOCCRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaOCCRCRCCAST(ParserG.ConsultaOCCRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitFiltroAST(ParserG.FiltroASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOFFAST(ParserG.TextoOFFASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaOFFAST(ParserG.ExpresionlogicaOFFASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOOAAST(ParserG.TextoOOAASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributosOOAAST(ParserG.AtributosOOAASTContext ctx) {
        return null;
    }

    @Override
    public Object visitOrdenamientosAST(ParserG.OrdenamientosASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAgrupamientosAST(ParserG.AgrupamientosASTContext ctx) {
        return null;
    }

    @Override
    public Object visitMotorAST(ParserG.MotorASTContext ctx) {
        return null;
    }

    @Override
    public Object visitMongoOMAST(ParserG.MongoOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visitPostgresOMAST(ParserG.PostgresOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOMAST(ParserG.TextoOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visit(ParseTree parseTree) {
        return null;
    }

    @Override
    public Object visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public Object visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    @Override
    public Object visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
