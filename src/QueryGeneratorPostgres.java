import generated.ParserG;
import generated.ParserGBaseVisitor;




public class QueryGeneratorPostgres extends ParserGBaseVisitor {

    private StringBuffer genQuery;

    public QueryGeneratorPostgres(){
        genQuery=new StringBuffer();

    }
    @Override
    public Object visitProgramAST(ParserG.ProgramASTContext ctx) {
        return visit(ctx.consulta());
    }

    @Override
    public Object visitConsultaAST(ParserG.ConsultaASTContext ctx) {

        return visit(ctx.cuerpoconsulta());
    }

    @Override
    public Object visitCuerpoconsultaAST(ParserG.CuerpoconsultaASTContext ctx) {
        genQuery.append("select ");
        genQuery.append((String) visit(ctx.atributos()));
        genQuery.append(" from ");
        genQuery.append((String) visit(ctx.capas()));
        genQuery.append(" where ");
        genQuery.append((String) visit(ctx.filtro()));
        return genQuery.toString();
    }

    @Override
    public Object visitAtributosCCAST(ParserG.AtributosCCASTContext ctx) {
        return visit(ctx.listaatributos());
    }

    @Override
    public Object visitListaatributosACCAST(ParserG.ListaatributosACCASTContext ctx) {
        String atributos = "";
        atributos += ctx.atributo(0).getText();
        for(int i = 1; i < ctx.atributo().size() -1; i++){
            atributos += ", " + ctx.atributo(i).getText();
        }

        return atributos;
    }

    @Override
    public Object visitAtributoLAACCAST(ParserG.AtributoLAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoALAACCAST(ParserG.TextoALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitNumeroALAACCAST(ParserG.NumeroALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributocapaALAACCAST(ParserG.AtributocapaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaALAACCAST(ParserG.ConsultaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoexpresionALAACCAST(ParserG.CuerpoexpresionALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaALAACCAST(ParserG.ExpresionlogicaALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributocapaOAALAACCAST(ParserG.AtributocapaOAALAACCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionAST(ParserG.ExpresionASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCuerpoexpresionEAST(ParserG.CuerpoexpresionEASTContext ctx) {
        return null;
    }

    @Override
    public Object visitNombrecolumnaELAST(ParserG.NombrecolumnaELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionELAST(ParserG.ExpresionELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaoplogELAST(ParserG.ExpresionlogicaoplogELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaoplogELOLELAST(ParserG.ExpresionlogicaoplogELOLELASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapasAST(ParserG.CapasASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapavaciaCAST(ParserG.CapavaciaCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitRepeticioncapaCAST(ParserG.RepeticioncapaCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitRepeticioncapaRCCAST(ParserG.RepeticioncapaRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitCapaRCRCCAST(ParserG.CapaRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOCCRCRCCAST(ParserG.TextoOCCRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitConsultaOCCRCRCCAST(ParserG.ConsultaOCCRCRCCASTContext ctx) {
        return null;
    }

    @Override
    public Object visitFiltroAST(ParserG.FiltroASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOFFAST(ParserG.TextoOFFASTContext ctx) {
        return null;
    }

    @Override
    public Object visitExpresionlogicaOFFAST(ParserG.ExpresionlogicaOFFASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOOAAST(ParserG.TextoOOAASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAtributosOOAAST(ParserG.AtributosOOAASTContext ctx) {
        return null;
    }

    @Override
    public Object visitOrdenamientosAST(ParserG.OrdenamientosASTContext ctx) {
        return null;
    }

    @Override
    public Object visitAgrupamientosAST(ParserG.AgrupamientosASTContext ctx) {
        return null;
    }

    @Override
    public Object visitMotorAST(ParserG.MotorASTContext ctx) {

        return null;
    }

    @Override
    public Object visitMongoOMAST(ParserG.MongoOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visitPostgresOMAST(ParserG.PostgresOMASTContext ctx) {
        return null;
    }

    @Override
    public Object visitTextoOMAST(ParserG.TextoOMASTContext ctx) {
        return null;
    }
}